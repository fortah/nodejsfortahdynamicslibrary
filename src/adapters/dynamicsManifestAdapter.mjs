/**
 * @module "DynamicsManifestAdapter" class
 * @description Converting between Dynamics manifest and corresponding class
 */

"use strict";

import { DynamicsApplication } from "../application/dynamicsApplication.mjs";
import { DynamicsApplications } from "../application/dynamicsApplications.mjs";
import { DynamicsDependency } from "../application/dynamicsDependency.mjs";
import { DynamicsDependencies } from "../application/dynamicsDependencies.mjs";
import { DynamicsRange } from "../application/dynamicsRange.mjs";
import { DynamicsRanges } from "../application/dynamicsRanges.mjs";
import { DynamicsVersion } from "../application/dynamicsVersion.mjs";

export class DynamicsManifestAdapter {
    get onNewDynamicsApplication() { return this.mOnNewDynamicsApplication; }
    set onNewDynamicsApplication(pValue) { this.mOnNewDynamicsApplication = pValue; }
    get onNewDynamicsDependency() { return this.mOnNewDynamicsDependency; }
    set onNewDynamicsDependency(pValue) { this.mOnNewDynamicsDependency = pValue; }
    get onNewDynamicsRange() { return this.mOnNewDynamicsRange; }
    set onNewDynamicsRange(pValue) { this.mOnNewDynamicsRange = pValue; }

    constructor() {
        this.onNewDynamicsApplication = null;
        this.onNewDynamicsDependency = null;
        this.onNewDynamicsRange = null;
    }

    dynamicsApplicationsFromData(pData) {
        let object = new DynamicsApplications();
        if ((pData != null) && (Array.isArray(pData)))
            for (const dataItem of pData)
                object.push(this.dynamicsApplicationFromData(dataItem));
        return object;
    }     

    dynamicsApplicationFromData(pData) {
        let object = null;
        if (pData != null) {
            const version = DynamicsVersion.parse(pData.version);
            const dependencies = this.dynamicsDependenciesFromData(pData.dependencies);
            const ranges = this.dynamicsRangesFromData(Array.verify(pData.idRanges, [ pData.idRange ]));
            object = this.newDynamicsApplication(pData.id, pData.name, pData.publisher, version, dependencies, ranges);
        }
        return object;
    }
        
    newDynamicsApplication(pId, pName, pPublisher, pVersion, pDependencies, pRanges) {
        if (this.onNewDynamicsApplication)
            return this.onNewDynamicsApplication(pId, pName, pPublisher, pVersion, pDependencies, pRanges);
        else
            return new DynamicsApplication(pId, pName, pPublisher, pVersion, pDependencies, pRanges);
    }

    dynamicsDependenciesFromData(pData) {
        let object = new DynamicsDependencies();
        if ((pData != null) && (Array.isArray(pData)))
            for (const dataItem of pData)
                object.push(this.dynamicsDependencyFromData(dataItem));
        return object;
    }    

    dynamicsDependencyFromData(pData) {
        let object = null;
        if (pData != null) {
            const id = (pData.id ? pData.id : pData.appId);
            const version = DynamicsVersion.parse(pData.version);
            object = this.newDynamicsDependency(id, pData.name, pData.publisher, version);
        }
        return object;
    }

    newDynamicsDependency(pId, pName, pPubnlisher, pVersion) {
        if (this.onNewDynamicsDependency)
            return this.onNewDynamicsDependency(pId, pName, pPubnlisher, pVersion);
        else
            return new DynamicsDependency(pId, pName, pPubnlisher, pVersion);
    }

    dynamicsRangesFromData(pData) {
        let object = new DynamicsRanges();
        if ((pData != null) && (Array.isArray(pData)))
            for (const data of pData)
                object.push(this.dynamicsRangeFromData(data));
        return object;
    }    

    dynamicsRangeFromData(pData) {
        let object = null;
        if (pData != null)
            object = this.newDynamicsRange(pData.from, pData.to);
        return object;
    }    

    newDynamicsRange(pFrom, pTo) {
        if (this.onNewDynamicsRange)
            return this.onNewDynamicsRange(pFrom, pTo);
        else
            return new DynamicsRange(pFrom, pTo);
    }

    applyDynamicsApplicationToData(pDynamicsApplication, pData) {
        pData.id = pDynamicsApplication.id;
        this.applyDynamicsDependenciesToData(pDynamicsApplication.dependencies, pData.dependencies);
        if ("idRanges" in pData)
            this.applyDynamicsRangesToData(pDynamicsApplication.ranges, pData.idRanges);
        else
            if ((Array.isArray(pDynamicsApplication.ranges)) && (pDynamicsApplication.ranges.length > 0))
                this.applyDynamicsRangeToData(pDynamicsApplication.ranges[0], pData.idRange);
    }    

    applyDynamicsDependenciesToData(pDynamicsDependencies, pData) {
        if (pDynamicsDependencies)
            for (const dataItem of pData) {
                const dataItemId = dataItem.id ? dataItem.id : dataItem.appId;
                const dynamicsDependency = pDynamicsDependencies.find((lDynamicsDependency) => { return lDynamicsDependency.id === dataItemId});
                if (dynamicsDependency)
                    this.applyDynamicsDependencyToData(dynamicsDependency, dataItem);
            }
    }

    applyDynamicsDependencyToData(pDynamicsDependency, pData) {
        if ("id" in pData)
            pData.id = pDynamicsDependency.id;
        else
            pData.appId = pDynamicsDependency.id;
    }

    applyDynamicsRangesToData(pDynamicsRanges, pData) {
        pData.clear();
        if (pDynamicsRanges)
            for (const dynamicsRange of pDynamicsRanges) {
                let dataItem = this.dynamicsRangeToData(dynamicsRange);
                pData.push(dataItem);
            }
    }

    applyDynamicsRangeToData(pDynamicsRange, pData) {
        pData.from = pDynamicsRange.from;
        pData.to = pDynamicsRange.to;
    }

    dynamicsRangeToData(pDynamicsRange) {
        return {
            "from": pDynamicsRange.from,
            "to": pDynamicsRange.to
        };
    }    
}