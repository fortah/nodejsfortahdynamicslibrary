/**
 * @module "DynamicsApplications" class
 * @description Represents an array of Dynamics apps
 */

"use strict";

export class DynamicsApplications extends Array {
    constructor() {        
        super();
    }

    get(pId) {
        return this.find((lDynamicsApplication) => { return (lDynamicsApplication.id === pId); });
    }   
}
