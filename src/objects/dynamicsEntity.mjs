/**
 * @module "DynamicsEntity" class
 * @description Abstract class - base for objects and object fields
 */

"use strict";

export class DynamicsEntity {
    get no() { return this.mNo; }
    set no(pValue) { this.mNo = Number.verifyAsInteger(pValue); }
    get name() { return this.mName; }
    set name(pValue) { this.mName = String.verify(pValue).removeIfStartsWith("\"").removeIfEndsWith("\""); }

    constructor(pNo, pName) {
        this.no = pNo;
        this.name = pName;
    }

    toData() {
        let data = {};
        data.no = this.no;
        data.name = this.name;
        return data;
    }
}
