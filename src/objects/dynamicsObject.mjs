/**
 * @module "DynamicsObject" class
 * @description Represents Dynamics object
 */

"use strict";

import { DynamicsEntity } from "../objects/dynamicsEntity.mjs";
import { DynamicsObjectType } from "../objects/dynamicsObjectType.mjs";

export class DynamicsObject extends DynamicsEntity {
    get type() { return this.mType; }
    set type(pValue) { this.mType = DynamicsObjectType.parse(pValue); }
    get fields() { return this.mFields; }
    set fields(pValue) { this.mFields = pValue; }
    get extends() { return this.mExtends; }
    set extends(pValue) { this.mExtends = pValue; }

    get typeText() { return DynamicsObjectType.toString(this.type); }

    constructor(pType, pNo, pName, pFields, pExtends) {
        super(pNo, pName);
        this.type = pType;
        this.fields = pFields;
        this.extends = pExtends;
    }

    toString() {
        let string = `${this.type} ${this.no} "${this.name}"`;
        if (this.extends)
            string = `${string} (extends ${this.extends.toString()})`;
        return string;
    }

    toData() {
        let data = super.toData();
        data.type = this.type;
        if (this.fields)
            data.fields = this.fields.toData();
        if (this.extends)
            data.extends = this.extends.toData();
        return data;
    }

    fromData(pData) {
        super.fromData();
        if (pData != null) {
            this.type = pData.type;
            if (data.fields)
                this.fields = (new DynamicsFields()).fromData(data.fields);
            if (data.extends)
                this.extends = (new DynamicsObject()).fromData(data.extends);
        }
        return this;
    }
}