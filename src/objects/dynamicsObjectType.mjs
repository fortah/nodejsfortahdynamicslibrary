/**
 * @module "DynamicsObjectType" class
 * @description Enum represents Dynamics object type
 */

"use strict";

import { Enum } from "fortah-core-library";
import { EnumItem } from "fortah-core-library";

export class DynamicsObjectType {
    static get table() { return "table"; }
    static get tableExtension() { return "tableExtension"; }
    static get form() { return "form"; }
    static get report() { return "report"; }
    static get dataport() { return "dataport"; }
    static get codeunit() { return "codeunit"; }
    static get xmlPort() { return "xmlPort"; }
    static get manuSuite() { return "menuSuite"; }
    static get page() { return "page"; }
    static get pageExtension() { return "pageExtension"; }
    static get query() { return "query"; }
    static get enum() { return "enum"; }
    static get enumExtension() { return "enumExtension"; }
    static get profile() { return "profile"; }
    static get pagecustomization() { return "pageCustomization"; }
    static get interface() { return "interface"; }
    static get entitlement() { return "entitlement"; }
    static get permissionSet() { return "permissionSet"; }
    static get permissionSetExtension() { return "permissionSetExtension"; }
    static get controlAddIn() { return "controlAddIn"; }

    static get values() { return [
        new EnumItem(DynamicsObjectType.table, "table"),
        new EnumItem(DynamicsObjectType.tableExtension, "tableextension"),
        new EnumItem(DynamicsObjectType.form, "form"),
        new EnumItem(DynamicsObjectType.report, "report"),
        new EnumItem(DynamicsObjectType.dataport, "dataport"),
        new EnumItem(DynamicsObjectType.codeunit, "codeunit"),
        new EnumItem(DynamicsObjectType.xmlPort, "xmlport"),
        new EnumItem(DynamicsObjectType.manuSuite, "menusuite"),
        new EnumItem(DynamicsObjectType.page, "page"),
        new EnumItem(DynamicsObjectType.pageExtension, "pageextension"),
        new EnumItem(DynamicsObjectType.query, "query"),
        new EnumItem(DynamicsObjectType.enum, "enum"),
        new EnumItem(DynamicsObjectType.enumExtension, "enumextension"),
        new EnumItem(DynamicsObjectType.profile, "profile"),
        new EnumItem(DynamicsObjectType.interface, "interface"),
        new EnumItem(DynamicsObjectType.entitlement, "entitlement"),
        new EnumItem(DynamicsObjectType.permissionSet, "permissionset"),
        new EnumItem(DynamicsObjectType.permissionSetExtension, "permissionsetextension"),
        new EnumItem(DynamicsObjectType.controlAddIn, "controladdin")
    ]; }

    static parse(pText) {
        return Enum.parse(pText, DynamicsObjectType.values, DynamicsObjectType.name);
    }

    static toString(pValue) {
        return Enum.toString(pValue, DynamicsObjectType.values, DynamicsObjectType.name);
    }
}